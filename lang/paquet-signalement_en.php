<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-signalement?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'signalement_description' => 'The plugin Notification can report illegal content on the site.',
	'signalement_nom' => 'Notification',
	'signalement_slogan' => 'Notify illegal contents'
);
