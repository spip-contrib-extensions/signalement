<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/signalement?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_signalement' => 'No notification',

	// B
	'bouton_alerter' => 'Alert',
	'bouton_enlever_alerte' => 'Delete this alert',

	// E
	'explication_depublier_seuil' => 'How alert are needed so that an item is automatically unpublished.',
	'explication_notif_publication' => 'Select administrators who will be notified when a new alert is generated. If none are selected, all administrators will be notified.',
	'explication_select_motifs' => 'The reasons below will be displayed in a selector to specify the nature of the report.',

	// I
	'icone_invalider_signalement' => 'Invalid this notification',
	'icone_invalider_signalements' => 'Invalid those notifications',
	'icone_suivi_signalements' => 'Follow notifications',
	'icone_valider_signalement' => 'Valid this notification',
	'icone_valider_signalements' => 'Valid those notifications',
	'info_1_signalement' => '1 notification',
	'info_aucun_signalement' => 'No notification',
	'info_gauche_suivi_signalement' => 'This page allows you to moderate the notifications made ​​on the site',
	'info_moderation_confirmee_refuse' => 'The notification #@id_signalement@ has been invalidated',
	'info_moderation_deja_faite' => 'The notification #@id_signalement@ has been moderated as "@statut@".',
	'info_moderation_email_droit_insuffisant' => 'No author with the email @email@ has the appropriate permission.',
	'info_moderation_lien_titre' => 'Moderate this notification from the private area',
	'info_moderation_signalement_introuvable' => 'The notification @id@ is not found.',
	'info_nb_signalements' => '@nb@ notifications',
	'info_sans_motif' => 'No reason',
	'info_selectionner_signalement' => 'Select the notifications:',

	// L
	'label_depublier' => 'Unpublication',
	'label_depublier_long' => 'Automatically unpublish a content whose number of warning exceeds a threshold',
	'label_depublier_seuil' => 'Unpublication threshold',
	'label_mediabox' => 'MediaBox',
	'label_mediabox_long' => 'Do not use the MediaBox to display the form',
	'label_motif' => 'Alerte reason',
	'label_notif_publication' => 'Who is notified when there is a new notification',
	'label_select_motif' => 'Select your reason',
	'label_select_motifs' => 'Reasons to select',
	'label_texte' => 'Your reason',
	'lien_signalement_objet' => 'Notification about: ',
	'lien_vider_selection' => 'Unselect',
	'lien_voir_signalements_objet' => 'See the notifications concerning this object',

	// M
	'mail_lien_refuser_commentaire' => 'Invalidate this notification',
	'mail_ne_repondez_pas' => 'Do not reply to this email. The address of the content of the alert is:',
	'mail_titre_signalement' => 'Notification',
	'motif_option_actes_danger' => 'Dangerous or harmful acts (drugs, explosives, suicide, mutilation ...)',
	'motif_option_autre' => 'Other',
	'motif_option_droits_auteurs' => 'Violation of my rights (copyright, invasion of my privacy ...)',
	'motif_option_enfants' => 'Child abuse',
	'motif_option_haine' => 'Hate content (hate speech, harassment, racism, abuse of vulnerable people ...)',
	'motif_option_sexe' => 'Sexual content (sex, nudity ...)',
	'motif_option_spam' => 'Spam (advertising, scams / fraud, misleading content ...)',
	'motif_option_violence' => 'Violent or gory content (assault, incitement to violence ...)',

	// S
	'signalement' => 'Notification',
	'signalement_ajouter' => 'Alert about this content',
	'signalement_enlever' => 'Delete your alert',
	'signalements' => 'Notifications',
	'signalements_aucun' => 'None',
	'signalements_publies' => 'Validated',
	'signalements_refuses' => 'Invalidated',
	'signalements_tous' => 'All',

	// T
	'texte_signalements' => 'Notifications concerning:',
	'titre_cfg_signalement' => 'Configure the notifications',
	'titre_selection_action' => 'Selection',
	'titre_signalement_suivi' => 'Follow notifications',
	'titre_signalements' => 'Notifications',
	'tout_voir' => 'See all'
);
