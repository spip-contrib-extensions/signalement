<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/signalement.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_signalement' => 'Aucun signalement',

	// B
	'bouton_alerter' => 'Alerter',
	'bouton_enlever_alerte' => 'Enlever votre alerte',

	// E
	'explication_depublier_seuil' => 'À partir de combien d’alertes un élément est automatiquement dépublié.',
	'explication_notif_publication' => 'Sélectionnez les administrateurs qui seront notifiés lors d’un nouveau signalement. Si aucun n’est sélectionné, tous les administrateurs seront notifiés.',
	'explication_select_motifs' => 'Les motifs ci-dessous seront affichés dans un sélecteur afin de préciser la nature du signalement.',

	// I
	'icone_invalider_signalement' => 'Invalider ce signalement',
	'icone_invalider_signalements' => 'Invalider ces signalements',
	'icone_suivi_signalements' => 'Suivi des signalements',
	'icone_valider_signalement' => 'Valider ce signalement',
	'icone_valider_signalements' => 'Valider ces signalements',
	'info_1_signalement' => '1 signalement',
	'info_aucun_signalement' => 'Aucun signalement',
	'info_gauche_suivi_signalement' => 'Cette page vous permet de modérer les signalements réalisés sur le site',
	'info_moderation_confirmee_refuse' => 'Le signalement #@id_signalement@ a bien été invalidé',
	'info_moderation_deja_faite' => 'Le signalement #@id_signalement@ a déjà été modéré en "@statut@".',
	'info_moderation_email_droit_insuffisant' => 'Aucun auteur avec l’email @email@ n’a de droit suffisant.',
	'info_moderation_lien_titre' => 'Modérer ce signalement depuis l’espace privé',
	'info_moderation_signalement_introuvable' => 'Le signalement @id@ est introuvable.',
	'info_nb_signalements' => '@nb@ signalements',
	'info_sans_motif' => 'Sans motif',
	'info_selectionner_signalement' => 'Sélectionner les signalements :',

	// L
	'label_depublier' => 'Dépublication',
	'label_depublier_long' => 'Dépublier automatiquement un contenu dont le nombre d’alerte dépasse un certain seuil',
	'label_depublier_seuil' => 'Seuil de dépublication',
	'label_mediabox' => 'MediaBox',
	'label_mediabox_long' => 'Ne pas utiliser la MediaBox pour afficher le formulaire',
	'label_motif' => 'Le motif de votre alerte',
	'label_notif_publication' => 'Qui est notifié lors d’un nouveau signalement',
	'label_select_motif' => 'Sélectionner votre motif',
	'label_select_motifs' => 'Motifs à sélectionner',
	'label_texte' => 'Votre explication',
	'lien_signalement_objet' => 'Signalement sur : ',
	'lien_vider_selection' => 'Désélectionner',
	'lien_voir_signalements_objet' => 'Voir les signalements sur cet objet',

	// M
	'mail_lien_refuser_commentaire' => 'Invalider ce signalement',
	'mail_ne_repondez_pas' => 'Ne répondez pas à ce mail. L’adresse du contenu de l’alerte est :',
	'mail_titre_signalement' => 'Signalement',
	'motif_option_actes_danger' => 'Actes dangereux ou pernicieux (drogues, explosifs, suicides, mutilation...)',
	'motif_option_autre' => 'Autre',
	'motif_option_droits_auteurs' => 'Violation de mes droits (droits d’auteurs, atteinte à ma vie privée...)',
	'motif_option_enfants' => 'Maltraitance d’enfants',
	'motif_option_haine' => 'Contenu incitant à la haine (apologie de la haine, harcèlement, racisme, abus sur personnes vulnérables...)',
	'motif_option_sexe' => 'Contenu à caractère sexuel (acte sexuel, nudité...)',
	'motif_option_spam' => 'Spam (publicité, arnaques/fraudes, contenu mensonger...)',
	'motif_option_violence' => 'Contenu violent ou sanglant (agression, incitation à la violence...)',

	// S
	'signalement' => 'Signalement',
	'signalement_ajouter' => 'Alerter sur ce contenu',
	'signalement_enlever' => 'Enlever votre alerte',
	'signalements' => 'Signalements',
	'signalements_aucun' => 'Aucun',
	'signalements_publies' => 'Validés',
	'signalements_refuses' => 'Invalidés',
	'signalements_tous' => 'Tous',

	// T
	'texte_signalements' => 'Les signalements de :',
	'titre_cfg_signalement' => 'Configurer les signalements',
	'titre_selection_action' => 'Sélection',
	'titre_signalement_suivi' => 'Suivi des signalements',
	'titre_signalements' => 'Signalements',
	'tout_voir' => 'Tout voir'
);
